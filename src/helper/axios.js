import axios from "axios";

const API_KEY = "f62f750b70a8ef11dad44670cfb6aa57";

const axiosInstance = axios.create({
  baseURL: "https://api.themoviedb.org/3/",
  params: {
    api_key: API_KEY,
  },
});
export default axiosInstance;
