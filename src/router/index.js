import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "movies",
    component: () =>
      import(/* webpackChunkName: "movies" */ "../pages/movies/Index.vue"),
  },
  {
    path: "/movies/:id",
    name: "movie",
    component: () =>
      import(/* webpackChunkName: "movie" */ "../pages/movies/_id.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
