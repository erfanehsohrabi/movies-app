import Vue from "vue";
import Vuex from "vuex";
import movies from "./modules/movies";
import VuexPersistence from "vuex-persist";
import "@mdi/font/css/materialdesignicons.min.css";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    movies,
  },
  plugins: [new VuexPersistence().plugin],
});
