export default {
  getMovies: (state) => {
    return state.movies;
  },
  getGenres: (state) => {
    return state.genres;
  },
  getCurrentPage: (state) => {
    return state.currentPage;
  },
  getDateRange: (state) => {
    return state.releaseDate;
  },
  getMovieById: (state) => (id) => {
    return state.movies.find((e) => e._id == id);
  },
  getImageUrl: () => (imageSize, imageSrc) => {
    return "https://image.tmdb.org/t/p/" + imageSize + "/" + imageSrc;
  },
};
