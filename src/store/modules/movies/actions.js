import axiosInstance from "@/helper/axios";
const MOVIES_ROUTE = "discover/movie";
const MOVIE_ROUTE = "movie/";
const GENRES_ROUTE = "genre/movie/list";
export function getAllMovies({ commit }, { page, releaseDate }) {
  return new Promise((resolve, reject) => {
    var params = {
      page,
      "release_date.gte": changeDateFormat(releaseDate.startDate),
      "release_date.lte": changeDateFormat(releaseDate.endDate),
    };
    if (releaseDate.startDate != "") {
      params.with_release_type = 2;
      params.region = "US";
    }
    axiosInstance
      .get(MOVIES_ROUTE, { params })
      .then(({ data }) => {
        commit("SET_MOVIES", data.results);
        resolve(data);
      })
      .catch((error) => reject(error));
  });
}
export function getAllGenres({ commit }) {
  return new Promise((resolve, reject) => {
    axiosInstance
      .get(GENRES_ROUTE)
      .then(({ data }) => {
        commit("SET_GENRES", data.genres);
        resolve(data);
      })
      .catch((error) => reject(error));
  });
}
// eslint-disable-next-line no-unused-vars
export function getMovie({ commit }, id) {
  return new Promise((resolve, reject) => {
    axiosInstance
      .get(MOVIE_ROUTE + id)
      .then(({ data }) => {
        resolve(data);
      })
      .catch((error) => reject(error));
  });
}
// eslint-disable-next-line no-unused-vars
export function getMovieCredits({ commit }, id) {
  return new Promise((resolve, reject) => {
    axiosInstance
      .get(MOVIE_ROUTE + id + "/credits")
      .then(({ data }) => {
        resolve(data);
      })
      .catch((error) => reject(error));
  });
}

function changeDateFormat(date) {
  if (date != "") {
    return new Date(date).toISOString().split("T")[0];
  } else {
    return "";
  }
}
