export function SET_MOVIES(state, data) {
  state.movies = data;
}
export function SET_PAGINATE(state, data) {
  state.currentPage = data;
}
export function SET_RELEASE_DATE(state, data) {
  state.releaseDate.startDate = data.startDate;
  state.releaseDate.endDate = data.endDate;
}
export function SET_GENRES(state, data) {
  state.genres = data;
}
