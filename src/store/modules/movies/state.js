export default function () {
  return {
    movies: [],
    genres: [],
    currentPage: 1,
    releaseDate: {
      startDate: "",
      endDate: "",
    },
  };
}
